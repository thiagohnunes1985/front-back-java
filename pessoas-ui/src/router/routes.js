
const routes = [
  {
    path: '/',
    component: () => import('layouts/layout-default.vue'),
    children: [
      { path: '', component: () => import('pages/pessoas/pessoa-dashboard') },
      {
        path: '/pessoas',
        name: 'pessoa-admin',
        component: () => import('pages/pessoas/pessoa-admin')
      },
      {
        path: '/pessoas/dashboard',
        name: 'pessoa-dashboard',
        component: () => import('pages/pessoas/pessoa-dashboard')
      },
      {
        path: '/pessoas/form',
        name: 'pessoa-formulario',
        component: () => import('pages/pessoas/pessoa-form')
      }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
