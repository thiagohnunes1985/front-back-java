package br.com.neppo;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import br.com.neppo.domain.model.dto.ConsultaCepDto;
import br.com.neppo.domain.service.ViaCep;

public class ConsultaCepTest {
	
	private ConsultaCepDto cepDto;
	
	@Before
	public void createDto() {
		this.cepDto = new ConsultaCepDto();
		cepDto.setCep("15076-610");
		cepDto.setLogradouro("Rua Doutor José Jorge Cury");
		cepDto.setComplemento("");
		cepDto.setBairro("Parque Industrial Tancredo Neves");
		cepDto.setLocalidade("São José do Rio Preto");
		cepDto.setUf("SP");
	}
	
	@Test
	public void deveTestarConsultaDeCep() {	
		ViaCep consulta = new ViaCep();
		consulta.setUrlBase("https://viacep.com.br/ws/{cep}/json");
		ConsultaCepDto dto = consulta.obterCep("15076610");
		assertEquals(dto, this.cepDto);
	}
}
