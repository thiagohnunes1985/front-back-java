package br.com.neppo.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import br.com.neppo.domain.service.ConsultaCep;
import br.com.neppo.domain.service.ViaCep;

@Configuration
public class ServiceCepConfig {
	
	@Bean
	public ConsultaCep getConsultaCepFactory() {
		return new ViaCep();
	}	
}
