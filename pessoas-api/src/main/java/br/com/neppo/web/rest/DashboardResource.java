package br.com.neppo.web.rest;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import br.com.neppo.domain.model.dto.ResumoSexo;
import br.com.neppo.domain.repository.Pessoas;

@RestController
@RequestMapping("dashboard")
public class DashboardResource {

	@Autowired
	private Pessoas pessoas;
		
	@GetMapping("resumo-sexo")
	public ResponseEntity<List<ResumoSexo>> getResumoPorSexo() {
		return ResponseEntity.ok().body(this.pessoas.getResumoPorSexo());
	}
	
	@GetMapping("resumo-faixa-etaria")
	public ResponseEntity<List<Object[]>> getResumoFaixaEtaria() {
		return ResponseEntity.ok().body(this.pessoas.getResumoPorFaixaEtaria());
	}
}
