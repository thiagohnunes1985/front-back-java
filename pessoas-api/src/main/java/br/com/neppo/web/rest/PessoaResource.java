package br.com.neppo.web.rest;

import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import br.com.neppo.domain.model.Pessoa;
import br.com.neppo.domain.model.dto.PessoaDto;
import br.com.neppo.domain.repository.filters.PessoaFiltro;
import br.com.neppo.domain.service.PessoaService;

@RestController
@RequestMapping("pessoas")
public class PessoaResource {
	
	private PessoaService pessoaService;
	
	@Autowired
	public PessoaResource(PessoaService pessoaService) {
		this.pessoaService = pessoaService;		
	}

	@GetMapping
	public ResponseEntity<List<Pessoa>> getPessoas(PessoaFiltro pessoaFiltro) {
		return ResponseEntity.ok().body(this.pessoaService.getPessoas(pessoaFiltro));
	}
	
	@GetMapping("{id}")
	public ResponseEntity<Pessoa> getPessoaById(@PathVariable("id") Long id) {
		return ResponseEntity.ok().body(this.pessoaService.getPessoaById(id));
	}	
	
	@PostMapping
	public ResponseEntity<Pessoa> addPessoa(@RequestBody @Valid PessoaDto pessoaDto) {
		return new ResponseEntity<Pessoa>(this.pessoaService.save(pessoaDto), HttpStatus.CREATED);
	}	
	
	@PutMapping("{id}")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<Pessoa> updatePessoa(@PathVariable("id") Long id, @RequestBody @Valid PessoaDto pessoaDto) {
		return ResponseEntity.ok().body(this.pessoaService.update(id, pessoaDto));
	}
	
	@DeleteMapping("{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public ResponseEntity<?> deletePessoa(@PathVariable("id") Long id) {
		this.pessoaService.delete(id);
		return ResponseEntity.noContent().build();
	}
}
