package br.com.neppo.domain.service.exceptions;

import org.springframework.http.HttpStatus;
import br.com.neppo.error.exceptions.NeppoRunTimeException;

public class PessoaNaoEncontradaException extends NeppoRunTimeException {

	private static final long serialVersionUID = 1L;

	public PessoaNaoEncontradaException() {
		super("pessoa-0", HttpStatus.NOT_FOUND);
	}
}
