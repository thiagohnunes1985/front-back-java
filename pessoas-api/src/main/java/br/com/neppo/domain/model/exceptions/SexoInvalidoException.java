package br.com.neppo.domain.model.exceptions;

import org.springframework.http.HttpStatus;
import br.com.neppo.error.exceptions.NeppoRunTimeException;

public class SexoInvalidoException extends NeppoRunTimeException {
	
	private static final long serialVersionUID = 1L;

	public SexoInvalidoException() {
		super("pessoa-e2", HttpStatus.BAD_REQUEST);
	}
}
