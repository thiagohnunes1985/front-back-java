package br.com.neppo.domain.service;

import java.util.List;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import br.com.neppo.domain.model.Pessoa;
import br.com.neppo.domain.model.dto.PessoaDto;
import br.com.neppo.domain.repository.Pessoas;
import br.com.neppo.domain.repository.filters.PessoaFiltro;
import br.com.neppo.domain.service.exceptions.PessoaJaCadastradaException;
import br.com.neppo.domain.service.exceptions.PessoaNaoEncontradaException;

@Service
public class PessoaService {
	
	private Pessoas pessoas;

	@Autowired
	public PessoaService(Pessoas pessoas) {
		this.pessoas = pessoas;
	}

	public List<Pessoa> getPessoas(PessoaFiltro pessoaFiltro) {
		return this.pessoas.findAll();
	}

	public Pessoa getPessoaById(Long id) {
		return 
		this.pessoas.findById(id)
		.orElseThrow(
			() -> new PessoaNaoEncontradaException()
		);		
	}

	public Pessoa save(PessoaDto pessoaDto) {
		if(this.pessoaCadastrada(pessoaDto))
			throw new PessoaJaCadastradaException();
		return this.pessoas.save(pessoaDto.toPessoa());
	}

	private boolean pessoaCadastrada(PessoaDto pessoaDto) {
		return this.pessoas.findPessoaByRg(pessoaDto.getRg()).isPresent();
	}

	public Pessoa update(Long id, PessoaDto pessoaDto) {
		Pessoa pessoa = pessoaDto.toPessoa();
		Pessoa pesquisa = this.getPessoaById(id);
		
		BeanUtils.copyProperties(pessoa, pesquisa, "id");
		return this.pessoas.save(pesquisa);
	}

	public void delete(Long id) {
		this.pessoas.delete(
			this.getPessoaById(id)
		);
	}
}
