package br.com.neppo.domain.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.neppo.domain.model.Pessoa;
import br.com.neppo.domain.model.dto.ResumoSexo;

@Repository
public interface Pessoas extends JpaRepository<Pessoa, Long> {

	@Query(
		value="select new br.com.neppo.domain.model.dto.ResumoSexo (p.sexo, count(p.id)) from Pessoa p group by p.sexo"			
	)
	public List<ResumoSexo> getResumoPorSexo();
	
//	@Query(
//		value=	  "select 	case "
//				+ "				when (extract(year from current_date) - extract(year from p.nascimento)) between  0 and 10 then '0 a 9' "
//				+ "				when (extract(year from current_date) - extract(year from p.nascimento)) between 11 and 20 then '10 a 20' "
//				+ "				when (extract(year from current_date) - extract(year from p.nascimento)) between 21 and 30 then '21 a 30' "
//				+ "				when (extract(year from current_date) - extract(year from p.nascimento)) between 31 and 40 then '31 a 40' "
//				+ "				when (extract(year from current_date) - extract(year from p.nascimento)) > 40 then 'Mais de 40' "
//				+ "			end,"
//				+ "			count(p.id)"
//				+ "from	Pessoa p "
//				+ "group by (current_date - p.nascimento) "
//	)
//	public List<PessoaFaixaEtaria> getResumoPorFaixaEtaria();
	
	@Query(
		value=	  "select 	case "
				+ "				when (extract(year from current_date) - extract(year from p.nascimento)) between  0 and 10 then '0 a 9' "
				+ "				when (extract(year from current_date) - extract(year from p.nascimento)) between 11 and 20 then '10 a 20' "
				+ "				when (extract(year from current_date) - extract(year from p.nascimento)) between 21 and 30 then '21 a 30' "
				+ "				when (extract(year from current_date) - extract(year from p.nascimento)) between 31 and 40 then '31 a 40' "
				+ "				when (extract(year from current_date) - extract(year from p.nascimento)) > 40 then 'Mais de 40' "
				+ "			end,"
				+ "			count(p.id)"
				+ "from	pessoa p "
				+ "group by extract(year from current_date) - extract(year from p.nascimento) ",
				nativeQuery=true
	)
	public List<Object[]> getResumoPorFaixaEtaria();
	
	public Optional<Pessoa> findPessoaByRg(String rg);
}
