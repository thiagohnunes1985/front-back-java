package br.com.neppo.domain.model;

import br.com.neppo.domain.model.exceptions.SexoInvalidoException;

public enum Sexo {

	MASCULINO("M", "Masculino"), 
	FEMININO("F", "Feminino");

	private String sexo;
	private String descricao;

	Sexo(String sexo, String descricao) {
		this.sexo = sexo;
		this.descricao = descricao;
	}

	public String getSexo() {
		return this.sexo;
	}

	public String getDescricao() {
		return descricao;
	}

	public static Sexo byKey(String key) {
		
		if(key==null || key.isEmpty())
			return null;
		
		for(Sexo sexo : Sexo.values()) {
			if(key.equals(sexo.getSexo()))
				return sexo;
		}	
		
		throw new SexoInvalidoException();
	}
}
