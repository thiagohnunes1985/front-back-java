package br.com.neppo.domain.model.builders;

import br.com.neppo.domain.model.Endereco;

public class EnderecoBuilder {
	
	private Endereco endereco = new Endereco();
	
	public EnderecoBuilder uf(String uf) {
		this.endereco.setUf(uf);
		return this;
	}
	
	public EnderecoBuilder cep(String cep) {
		this.endereco.setCep(cep);
		return this;
	}
	
	public EnderecoBuilder municipio(String municipio) {
		this.endereco.setMunicipio(municipio);
		return this;
	}
	
	public EnderecoBuilder bairro(String bairro) {
		this.endereco.setBairro(bairro);
		return this;
	}
	
	public EnderecoBuilder numero(String numero) {
		this.endereco.setNumero(numero);
		return this;
	}
	
	public EnderecoBuilder complemento(String complemento) {
		this.endereco.setComplemento(complemento);
		return this;
	}
	
	public EnderecoBuilder logradouro(String logradouro) {
		this.endereco.setLogradouro(logradouro);
		return this;
	}

	public Endereco build() {
		return this.endereco;
	}	
}
