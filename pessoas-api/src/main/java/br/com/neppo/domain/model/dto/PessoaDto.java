package br.com.neppo.domain.model.dto;

import java.time.LocalDate;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonFormat;

import br.com.neppo.domain.model.Pessoa;
import br.com.neppo.domain.model.Sexo;
import br.com.neppo.domain.model.builders.PessoaBuilder;

public class PessoaDto {
	
	@NotBlank(message="pessoa-1")
	@Size(min=10, max=80, message="pessoa-2")
	private String nome;	
	
	@NotBlank(message="pessoa-3")
	@Size(max=30, message="pessoa-4")
	private String rg;
	
	@JsonFormat(pattern="yyyy-MM-dd")
	@NotNull(message="pessoa-5")
	private LocalDate nascimento;
	
	@NotBlank(message="pessoa-6")
	private String sexo;
		
	private ConsultaCepDto endereco;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public LocalDate getNascimento() {
		return nascimento;
	}

	public void setNascimento(LocalDate nascimento) {
		this.nascimento = nascimento;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	
	public ConsultaCepDto getEndereco() {
		return endereco;
	}

	public void setEndereco(ConsultaCepDto endereco) {
		this.endereco = endereco;
	}

	public Pessoa toPessoa() {
		return 
		new PessoaBuilder()
		.nome(nome)
		.rg(rg)
		.nascimento(nascimento)
		.sexo(Sexo.byKey(sexo))
		.endereco(
			this.endereco.toEndereco()
		)
		.build();
	}
}
