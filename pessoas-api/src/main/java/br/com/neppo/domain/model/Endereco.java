package br.com.neppo.domain.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import com.fasterxml.jackson.annotation.JsonProperty;

@Embeddable
public class Endereco {

	@Column(length=10)
	private String cep;
	
	@Column(length=2)
	private String uf;
	
	@Column(length=80)
	@JsonProperty(value="localidade")
	private String municipio;
	
	@Column(length=80)
	private String bairro;
	
	@Column(length=140)
	private String logradouro;
	
	@Column(length=40)
	private String complemento;
	
	@Column(length=10)
	private String numero;
	
	public Endereco() { }
	
	public Endereco(String cep, String uf, String municipio, String bairro, String logradouro, String complemento,
			String numero) {
		super();
		this.cep = cep;
		this.uf = uf;
		this.municipio = municipio;
		this.bairro = bairro;
		this.logradouro = logradouro;
		this.complemento = complemento;
		this.numero = numero;
	}

	public String getCep() {
		return cep;
	}
	
	public void setCep(String cep) {
		this.cep = cep;
	}
	
	public String getUf() {
		return uf;
	}
	
	public void setUf(String uf) {
		this.uf = uf;
	}
	public String getMunicipio() {
		return municipio;
	}
	
	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}
	
	public String getBairro() {
		return bairro;
	}
	
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	
	public String getLogradouro() {
		return logradouro;
	}
	
	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}
	
	public String getComplemento() {
		return complemento;
	}
	
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}
	
	public String getNumero() {
		return numero;
	}
	
	public void setNumero(String numero) {
		this.numero = numero;
	}

	@Override
	public String toString() {
		return "Endereco [cep=" + cep + ", uf=" + uf + ", municipio=" + municipio + ", bairro=" + bairro
				+ ", logradouro=" + logradouro + ", complemento=" + complemento + ", numero=" + numero + "]";
	}	
}
