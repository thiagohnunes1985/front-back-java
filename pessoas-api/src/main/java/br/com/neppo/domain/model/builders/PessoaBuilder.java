package br.com.neppo.domain.model.builders;

import java.time.LocalDate;
import br.com.neppo.domain.model.Endereco;
import br.com.neppo.domain.model.Pessoa;
import br.com.neppo.domain.model.Sexo;

public class PessoaBuilder {
	
	private Pessoa pessoa = new Pessoa();

	public PessoaBuilder nome(String nome) {
		this.pessoa.setNome(nome);
		return this;
	}
	
	public PessoaBuilder rg(String rg) {
		this.pessoa.setRg(rg);
		return this;
	}
	
	public PessoaBuilder sexo(Sexo sexo) {
		this.pessoa.setSexo(sexo);
		return this;
	}
	
	public PessoaBuilder nascimento(LocalDate nascimento) {
		this.pessoa.setNascimento(nascimento);
		return this;
	}
	
	public PessoaBuilder endereco(Endereco endereco) {
		this.pessoa.setEndereco(endereco);
		return this;
	}
	
	public Pessoa build() {
		return this.pessoa;
	}
}
