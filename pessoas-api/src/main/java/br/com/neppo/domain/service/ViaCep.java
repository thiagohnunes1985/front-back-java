package br.com.neppo.domain.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import br.com.neppo.domain.model.dto.ConsultaCepDto;
import br.com.neppo.domain.service.exceptions.CepInvalidParameter;

public class ViaCep implements ConsultaCep {
	
	@Value("${viaCep.urlBase}")
	private String urlBase;

	@Override
	public ConsultaCepDto obterCep(String cep) {
		if(cep==null || cep.isEmpty())
			throw new CepInvalidParameter();
		try {
			return new RestTemplate().getForObject(this.urlBase,ConsultaCepDto.class, cep).validade();
		}catch(HttpClientErrorException e) {
			throw new CepInvalidParameter();
		}
	}

	public void setUrlBase(String urlBase) {
		this.urlBase = urlBase;
	}	
}
