package br.com.neppo.domain.model.dto.exceptions;

import org.springframework.http.HttpStatus;
import br.com.neppo.error.exceptions.NeppoRunTimeException;

public class ConsultaCepInvalidObject extends NeppoRunTimeException {
	
	private static final long serialVersionUID = 1L;

	public ConsultaCepInvalidObject() {
		super("cep-1", HttpStatus.BAD_REQUEST);
	}
}
