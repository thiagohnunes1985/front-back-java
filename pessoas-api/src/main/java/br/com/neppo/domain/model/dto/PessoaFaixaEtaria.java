package br.com.neppo.domain.model.dto;

public class PessoaFaixaEtaria {
	
	private String faixa;
	private Long quantidade;
	
	public PessoaFaixaEtaria() {
		
	}
		
	public PessoaFaixaEtaria(String faixa, Long quantidade) {
		this.faixa = faixa;
		this.quantidade = quantidade;
	}

	public String getFaixa() {
		return faixa;
	}
	
	public void setFaixa(String faixa) {
		this.faixa = faixa;
	}
	
	public Long getQuantidade() {
		return quantidade;
	}
	
	public void setQuantidade(Long quantidade) {
		this.quantidade = quantidade;
	}

	@Override
	public String toString() {
		return "PessoaFaixaEtaria [faixa=" + faixa + ", quantidade=" + quantidade + "]";
	}
}
