package br.com.neppo.domain.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import br.com.neppo.domain.model.dto.ConsultaCepDto;

@RestController
@RequestMapping("cep")
public class ConsultaCepResource {

	@Autowired
	private ConsultaCep consultaCep;
	
	@GetMapping("{cep}")
	public ResponseEntity<ConsultaCepDto> getCep(@PathVariable("cep") String cep) {
		return ResponseEntity.ok().body(this.consultaCep.obterCep(cep));
	}	
}
