package br.com.neppo.domain.model.dto;

public class ResumoSexo {
	
	private String sexo;
	private Long quantidade;
	
	public ResumoSexo() {
		
	}
	
	public ResumoSexo(String sexo, Long quantidade) {
		super();
		this.sexo = sexo;
		this.quantidade = quantidade;
	}

	public String getSexo() {
		return sexo.equals("M") ? "Masculino":"Feminino";
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public Long getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Long quantidade) {
		this.quantidade = quantidade;
	}

	@Override
	public String toString() {
		return "ResumoSexo [sexo=" + sexo + ", quantidade=" + quantidade + "]";
	}
}
