package br.com.neppo.domain.service.exceptions;

import org.springframework.http.HttpStatus;
import br.com.neppo.error.exceptions.NeppoRunTimeException;

public class CepInvalidParameter extends NeppoRunTimeException {

	private static final long serialVersionUID = 1L;
	
	public CepInvalidParameter() {
		super("cep-1", HttpStatus.BAD_REQUEST);
	}
}
