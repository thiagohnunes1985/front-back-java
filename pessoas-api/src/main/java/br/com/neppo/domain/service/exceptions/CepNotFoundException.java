package br.com.neppo.domain.service.exceptions;

import org.springframework.http.HttpStatus;
import br.com.neppo.error.exceptions.NeppoRunTimeException;

public class CepNotFoundException extends NeppoRunTimeException {

	private static final long serialVersionUID = 1L;
	
	public CepNotFoundException() {
		super("cep-2", HttpStatus.NOT_FOUND);
	}	
}
