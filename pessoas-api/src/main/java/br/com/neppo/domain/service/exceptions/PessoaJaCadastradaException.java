package br.com.neppo.domain.service.exceptions;

import org.springframework.http.HttpStatus;

import br.com.neppo.error.exceptions.NeppoRunTimeException;

public class PessoaJaCadastradaException extends NeppoRunTimeException {

	private static final long serialVersionUID = 1L;
	
	public PessoaJaCadastradaException() {
		super("pessoa-e1", HttpStatus.CONFLICT);
	}

}
