package br.com.neppo.domain.service;

import br.com.neppo.domain.model.dto.ConsultaCepDto;

public interface ConsultaCep {
	
	public ConsultaCepDto obterCep(String cep);

}
