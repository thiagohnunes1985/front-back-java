package br.com.neppo.error.exceptions;

import org.springframework.http.HttpStatus;

public abstract class NeppoRunTimeException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	private final String errorCode;
	private final HttpStatus httpStatusCode;
	
	public NeppoRunTimeException(String errorCode, HttpStatus httpStatusCode) {
		this.errorCode = errorCode;
		this.httpStatusCode = httpStatusCode;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public HttpStatus getHttpStatusCode() {
		return httpStatusCode;
	}	
}
