package br.com.neppo.error;

import static java.util.stream.Collectors.toList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Stream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import br.com.neppo.error.ErrorResponse.ApiError;
import br.com.neppo.error.exceptions.NeppoRunTimeException;

@RestControllerAdvice
public class ApiExceptionHandler {
	
	private static final String NO_MESSAGE_AVAILABLE = "No message available";
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ApiExceptionHandler.class);
	
	@Autowired
	private MessageSource apiErrorMessageSource;
	
	@ExceptionHandler(NeppoRunTimeException.class)
    public ResponseEntity<ErrorResponse> handleBusinessExceptions(NeppoRunTimeException exception, Locale locale) {
        final String errorCode = exception.getErrorCode();
        final HttpStatus status = exception.getHttpStatusCode();
        return createResponse(errorCode, status, locale);
    }
	
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity<ErrorResponse> 
	handleValidationExceptions(MethodArgumentNotValidException exception, Locale locale) {
		
		Stream<ObjectError> errors = exception.getBindingResult().getAllErrors().stream();
		
		List<ApiError> apiErrors = errors
				.map(ObjectError::getDefaultMessage)
				.map(code->toApiError(code, locale))
				.collect(toList());
		
		return ResponseEntity.badRequest().body(ErrorResponse.of(HttpStatus.BAD_REQUEST, apiErrors));		
	}
	
	@ExceptionHandler(InvalidFormatException.class)
	public ResponseEntity<ErrorResponse> handleInvalidFormatException(InvalidFormatException exception, Locale locale) {
		final String errorCode = "generic-1";
		final HttpStatus status = HttpStatus.BAD_REQUEST;
		final ErrorResponse errorResponse = ErrorResponse.of(status, toApiError(errorCode, locale, exception.getValue()));
		return ResponseEntity.status(status).body(errorResponse);
	}
	
	@ExceptionHandler(Exception.class)
	public ResponseEntity<ErrorResponse> handleInternalServerError(Exception exception, Locale locale) {
		LOGGER.error("Error not expected", exception);
		final String errorCode = "error-1";
		final HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
		final ErrorResponse errorResponse = ErrorResponse.of(status, toApiError(errorCode, locale));
		return ResponseEntity.status(status).body(errorResponse);
	}
	
	private ResponseEntity<ErrorResponse> createResponse(
            String error, HttpStatus status, Locale locale) {
        final ErrorResponse errorResponse = ErrorResponse.of(status,
                toApiError(error, locale));
        return ResponseEntity.status(status).body(errorResponse);
    }
	
	private ApiError toApiError(String code, Locale locale, Object... args) {
		String message;
		try {
			message = apiErrorMessageSource.getMessage(code, args, locale);
		} catch (NoSuchMessageException e){
			LOGGER.error("Couldn't find any message for {} code under {} locale", code);
			message=NO_MESSAGE_AVAILABLE;
		}
		return new ApiError(code, message);
	}
}
