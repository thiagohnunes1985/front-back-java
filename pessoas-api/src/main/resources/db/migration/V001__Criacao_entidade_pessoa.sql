create table pessoa (
	id			serial		not null,
	nome		varchar(80)	not null,
	nascimento	date		not null,
    rg          varchar(30) not null,
	sexo		varchar(1)	not null,
	cep			varchar(10)	not null,
	uf			varchar(2)	not null,
	municipio	varchar(80)	not null,
	bairro		varchar(80)	not null,
	logradouro	varchar(80)	not null,
	complemento	varchar(40)	not null,
	numero		varchar(10)	not null,	
	constraint pk_pessoa primary key (id)
);
